﻿namespace TicTacToe
{
    partial class FormSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSetup));
            this.groupBoxGameMode = new System.Windows.Forms.GroupBox();
            this.radioButtonMultiplayer = new System.Windows.Forms.RadioButton();
            this.radioButtonSingleplayer = new System.Windows.Forms.RadioButton();
            this.groupBoxColor = new System.Windows.Forms.GroupBox();
            this.pictureBoxPlayer2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxPlayer1 = new System.Windows.Forms.PictureBox();
            this.labelChooseColorPlayer2 = new System.Windows.Forms.Label();
            this.labelColorPlayer1 = new System.Windows.Forms.Label();
            this.colorDialogChooseColor = new System.Windows.Forms.ColorDialog();
            this.buttonClose = new System.Windows.Forms.Button();
            this.groupBoxGameMode.SuspendLayout();
            this.groupBoxColor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPlayer2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPlayer1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxGameMode
            // 
            this.groupBoxGameMode.Controls.Add(this.radioButtonMultiplayer);
            this.groupBoxGameMode.Controls.Add(this.radioButtonSingleplayer);
            this.groupBoxGameMode.Location = new System.Drawing.Point(13, 13);
            this.groupBoxGameMode.Name = "groupBoxGameMode";
            this.groupBoxGameMode.Size = new System.Drawing.Size(197, 70);
            this.groupBoxGameMode.TabIndex = 0;
            this.groupBoxGameMode.TabStop = false;
            this.groupBoxGameMode.Text = "GameMode";
            // 
            // radioButtonMultiplayer
            // 
            this.radioButtonMultiplayer.AutoSize = true;
            this.radioButtonMultiplayer.Location = new System.Drawing.Point(24, 42);
            this.radioButtonMultiplayer.Name = "radioButtonMultiplayer";
            this.radioButtonMultiplayer.Size = new System.Drawing.Size(75, 17);
            this.radioButtonMultiplayer.TabIndex = 1;
            this.radioButtonMultiplayer.TabStop = true;
            this.radioButtonMultiplayer.Text = "Multiplayer";
            this.radioButtonMultiplayer.UseVisualStyleBackColor = true;
            this.radioButtonMultiplayer.CheckedChanged += new System.EventHandler(this.radioButtonMultiplayer_CheckedChanged);
            // 
            // radioButtonSingleplayer
            // 
            this.radioButtonSingleplayer.AutoSize = true;
            this.radioButtonSingleplayer.Location = new System.Drawing.Point(24, 19);
            this.radioButtonSingleplayer.Name = "radioButtonSingleplayer";
            this.radioButtonSingleplayer.Size = new System.Drawing.Size(82, 17);
            this.radioButtonSingleplayer.TabIndex = 0;
            this.radioButtonSingleplayer.TabStop = true;
            this.radioButtonSingleplayer.Text = "Singleplayer";
            this.radioButtonSingleplayer.UseVisualStyleBackColor = true;
            this.radioButtonSingleplayer.CheckedChanged += new System.EventHandler(this.radioButtonSingleplayer_CheckedChanged);
            // 
            // groupBoxColor
            // 
            this.groupBoxColor.Controls.Add(this.pictureBoxPlayer2);
            this.groupBoxColor.Controls.Add(this.pictureBoxPlayer1);
            this.groupBoxColor.Controls.Add(this.labelChooseColorPlayer2);
            this.groupBoxColor.Controls.Add(this.labelColorPlayer1);
            this.groupBoxColor.Location = new System.Drawing.Point(231, 13);
            this.groupBoxColor.Name = "groupBoxColor";
            this.groupBoxColor.Size = new System.Drawing.Size(219, 70);
            this.groupBoxColor.TabIndex = 1;
            this.groupBoxColor.TabStop = false;
            this.groupBoxColor.Text = "Color";
            // 
            // pictureBoxPlayer2
            // 
            this.pictureBoxPlayer2.BackColor = System.Drawing.Color.Red;
            this.pictureBoxPlayer2.Location = new System.Drawing.Point(18, 42);
            this.pictureBoxPlayer2.Name = "pictureBoxPlayer2";
            this.pictureBoxPlayer2.Size = new System.Drawing.Size(16, 16);
            this.pictureBoxPlayer2.TabIndex = 3;
            this.pictureBoxPlayer2.TabStop = false;
            this.pictureBoxPlayer2.Click += new System.EventHandler(this.pictureBoxPlayer2_Click);
            // 
            // pictureBoxPlayer1
            // 
            this.pictureBoxPlayer1.BackColor = System.Drawing.Color.Blue;
            this.pictureBoxPlayer1.Location = new System.Drawing.Point(18, 19);
            this.pictureBoxPlayer1.Name = "pictureBoxPlayer1";
            this.pictureBoxPlayer1.Size = new System.Drawing.Size(16, 16);
            this.pictureBoxPlayer1.TabIndex = 2;
            this.pictureBoxPlayer1.TabStop = false;
            this.pictureBoxPlayer1.Click += new System.EventHandler(this.pictureBoxPlayer1_Click);
            // 
            // labelChooseColorPlayer2
            // 
            this.labelChooseColorPlayer2.Location = new System.Drawing.Point(40, 44);
            this.labelChooseColorPlayer2.Name = "labelChooseColorPlayer2";
            this.labelChooseColorPlayer2.Size = new System.Drawing.Size(100, 17);
            this.labelChooseColorPlayer2.TabIndex = 1;
            this.labelChooseColorPlayer2.Text = "Player 2";
            // 
            // labelColorPlayer1
            // 
            this.labelColorPlayer1.Location = new System.Drawing.Point(40, 21);
            this.labelColorPlayer1.Name = "labelColorPlayer1";
            this.labelColorPlayer1.Size = new System.Drawing.Size(100, 17);
            this.labelColorPlayer1.TabIndex = 0;
            this.labelColorPlayer1.Text = "Player 1";
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(375, 89);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 19);
            this.buttonClose.TabIndex = 2;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // FormSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(466, 113);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.groupBoxColor);
            this.Controls.Add(this.groupBoxGameMode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormSetup";
            this.ShowInTaskbar = false;
            this.Text = "Setup";
            this.groupBoxGameMode.ResumeLayout(false);
            this.groupBoxGameMode.PerformLayout();
            this.groupBoxColor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPlayer2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPlayer1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxGameMode;
        private System.Windows.Forms.RadioButton radioButtonMultiplayer;
        private System.Windows.Forms.RadioButton radioButtonSingleplayer;
        private System.Windows.Forms.GroupBox groupBoxColor;
        private System.Windows.Forms.Label labelChooseColorPlayer2;
        private System.Windows.Forms.Label labelColorPlayer1;
        private System.Windows.Forms.ColorDialog colorDialogChooseColor;
        private System.Windows.Forms.PictureBox pictureBoxPlayer2;
        private System.Windows.Forms.PictureBox pictureBoxPlayer1;
        private System.Windows.Forms.Button buttonClose;
    }
}