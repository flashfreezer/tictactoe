﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class FormSetup : Form
    {
        FormMain frmFormMain;

        public FormSetup(FormMain MainForm)
        {
            InitializeComponent();
            frmFormMain = MainForm;
            MainForm.coPlayer1 = pictureBoxPlayer1.BackColor;
            MainForm.coPlayer2 = pictureBoxPlayer2.BackColor;
            if (frmFormMain.iGameMode == 1)
            {
                radioButtonMultiplayer.Checked = true;
                radioButtonSingleplayer.Checked = false;
            }
            if (frmFormMain.iGameMode == 2)
            {
                radioButtonSingleplayer.Checked = true;
                radioButtonMultiplayer.Checked = false;
            }
            
        }
       
        private void pictureBoxPlayer1_Click(object sender, EventArgs e)
        {
            if(colorDialogChooseColor.ShowDialog() == DialogResult.OK)
               {
                    pictureBoxPlayer1.BackColor=colorDialogChooseColor.Color;
                    frmFormMain.coPlayer1 = pictureBoxPlayer1.BackColor;
               }
        }
        private void pictureBoxPlayer2_Click(object sender, EventArgs e)
        {
            if (colorDialogChooseColor.ShowDialog() == DialogResult.OK)
            {
                pictureBoxPlayer2.BackColor = colorDialogChooseColor.Color;
                frmFormMain.coPlayer2 = pictureBoxPlayer2.BackColor;
            }
        }
        private void buttonClose_Click(object sender, EventArgs e)
        {
           Close();
        }
        private void radioButtonMultiplayer_CheckedChanged(object sender, EventArgs e)
        {
              groupBoxColor.Enabled = true;
              frmFormMain.iGameMode = 1;
              pictureBoxPlayer2.Enabled = true;
              labelChooseColorPlayer2.Enabled = true;
              //buttonClose.Location = new System.Drawing.Point(375,89);
        }
        private void radioButtonSingleplayer_CheckedChanged(object sender, EventArgs e)
        {
             groupBoxColor.Enabled = true;
             pictureBoxPlayer2.Enabled = false;
             labelChooseColorPlayer2.Enabled = false;
             frmFormMain.iGameMode = 2;
             //buttonClose.Location = new System.Drawing.Point(135, 89);
        }
    }
}