﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

/*
Tic Tac Toe with Computer - Visual C# 2011 (.Net 4.0)
created 2011 by Matthias Lexer
*/

namespace TicTacToe
{
    public partial class FormMain : Form
    {
        PictureBox oActualBox;
        PictureBox oActualComputerBox;
        int iActualPlayer;
        Color coActualColor;
        public Color coPlayer1;
        public Color coPlayer2;
        Color coComputer;
        public int iGameMode;
        int iZahl11, iZahl12, iZahl13, iZahl21, iZahl22, iZahl23, iZahl31, iZahl32, iZahl33 = 0;
        Form FormAbout;
        Form FormSetup;

        public FormMain()
        {
            InitializeComponent();
            oActualBox = new PictureBox();
            oActualComputerBox = new PictureBox();
            iActualPlayer = 1;                      // wenn 1 dann Spieler 1 , wenn 2 dann Spieler 2, wenn 3 dann 
            iGameMode = 1;
            coPlayer1 = Color.Blue;
            coPlayer2 = Color.Red;
            coComputer = Color.Green;
            FormAbout = new FormAbout();
            StatBoxes();
            NewGame();
        }
        void StatBoxes()
        {
            pictureBoxPlayer1.BackColor = coPlayer1;
            if (iGameMode == 1)
            {
                pictureBoxPlayer2.BackColor = coPlayer2;
                labelPlayer2.Text = "Player 2";
            }
            else
            {
                pictureBoxPlayer2.BackColor = coComputer;
                labelPlayer2.Text = "Computer";
            }
        }
        void ClickAction(PictureBox PictureBoxActual)
        {
            //Hier wird der mitgelieferte Wert als die neue ausgewählte oActualBox ausgewählt
            oActualBox = PictureBoxActual;
            oActualBox.Enabled = false;

            if (iActualPlayer == 1)
            {
                coActualColor = coPlayer1;
                oActualBox.BackColor = coActualColor;
                if (iGameMode == 2)
                {
                    if (WinOrLos(coActualColor) == true)
                    {
                        labelStatus.BackColor = Color.Gold;
                        AllPictueBoxesDeactivate();

                        if (iActualPlayer == 1)
                        {
                            labelStatus.Text = "Player 1 wins";
                        }
                    }
                    else
                    {
                        iActualPlayer = iActualPlayer + 2;
                        labelStatus.BackColor = coComputer;
                        labelStatus.Text = "Computer's turn";
                    }
                }
                if (iGameMode == 1)
                {
                    iActualPlayer = iActualPlayer + 1;      //wenn 1 dann wird sie zu 2 
                    labelStatus.BackColor = coPlayer2;
                    labelStatus.Text = "It's Player's 2 turn";
                }
                oActualBox.BackColor = coActualColor;
            }
            else
            {
                if (iActualPlayer == 2)
                {
                    coActualColor = coPlayer2;
                    iActualPlayer = iActualPlayer - 1;       //wenn 2 dann wird sie zu 1
                    labelStatus.BackColor = coPlayer1;
                    labelStatus.Text = "It's Player's 1 turn";
                    oActualBox.BackColor = coActualColor;
                }
            }
            if (iActualPlayer == 3)
            {
                coActualColor = coComputer;
                labelStatus.BackColor = coPlayer1;
                labelStatus.Text = "It's Player's 1 turn";
                ComputerPlayer();
            }


            if (WinOrLos(coActualColor) == true)
            {
                labelStatus.BackColor = Color.Gold;
                AllPictueBoxesDeactivate();

                if (iGameMode == 1)
                {
                    if (iActualPlayer == 1)
                    {
                        labelStatus.Text = "Player 2 wins";
                    }
                    if (iActualPlayer == 2)
                    {
                        labelStatus.Text = "Player 1 wins";
                    }
                }

                if (iGameMode == 2)
                {
                    if (iActualPlayer == 1)
                    {
                        labelStatus.Text = "Player 1 wins";
                    }
                    if (iActualPlayer == 3)
                    {
                        labelStatus.Text = "Computer wins";
                    }
                }
            }

            else
            {
                if (iActualPlayer == 3)
                {
                    iActualPlayer = iActualPlayer - 2;
                }
                else
                {
                    if (pictureBoxFeld1.BackColor != Color.White &&
                    pictureBoxFeld2.BackColor != Color.White &&
                    pictureBoxFeld3.BackColor != Color.White &&
                    pictureBoxFeld4.BackColor != Color.White &&
                    pictureBoxFeld5.BackColor != Color.White &&
                    pictureBoxFeld6.BackColor != Color.White &&
                    pictureBoxFeld7.BackColor != Color.White &&
                    pictureBoxFeld8.BackColor != Color.White &&
                    pictureBoxFeld9.BackColor != Color.White)
                    {
                        labelStatus.Text = "Unentschieden";
                        labelStatus.BackColor = Color.Silver;
                        AllPictueBoxesDeactivate();
                    }
                }

            }
        }
        void AllPictueBoxesDeactivate()
        {
            pictureBoxFeld1.Enabled = false;
            pictureBoxFeld2.Enabled = false;
            pictureBoxFeld3.Enabled = false;
            pictureBoxFeld4.Enabled = false;
            pictureBoxFeld5.Enabled = false;
            pictureBoxFeld6.Enabled = false;
            pictureBoxFeld7.Enabled = false;
            pictureBoxFeld8.Enabled = false;
            pictureBoxFeld9.Enabled = false;
        }
        bool WinOrLos(Color coActualColor)
        {
            if ((pictureBoxFeld1.BackColor == coActualColor) && (pictureBoxFeld2.BackColor == coActualColor) && (pictureBoxFeld3.BackColor == coActualColor)) return true;
            if ((pictureBoxFeld4.BackColor == coActualColor) && (pictureBoxFeld5.BackColor == coActualColor) && (pictureBoxFeld6.BackColor == coActualColor)) return true;
            if ((pictureBoxFeld7.BackColor == coActualColor) && (pictureBoxFeld8.BackColor == coActualColor) && (pictureBoxFeld9.BackColor == coActualColor)) return true;
            if ((pictureBoxFeld1.BackColor == coActualColor) && (pictureBoxFeld4.BackColor == coActualColor) && (pictureBoxFeld7.BackColor == coActualColor)) return true;
            if ((pictureBoxFeld2.BackColor == coActualColor) && (pictureBoxFeld5.BackColor == coActualColor) && (pictureBoxFeld8.BackColor == coActualColor)) return true;
            if ((pictureBoxFeld3.BackColor == coActualColor) && (pictureBoxFeld6.BackColor == coActualColor) && (pictureBoxFeld9.BackColor == coActualColor)) return true;
            if ((pictureBoxFeld1.BackColor == coActualColor) && (pictureBoxFeld5.BackColor == coActualColor) && (pictureBoxFeld9.BackColor == coActualColor)) return true;
            if ((pictureBoxFeld3.BackColor == coActualColor) && (pictureBoxFeld5.BackColor == coActualColor) && (pictureBoxFeld7.BackColor == coActualColor)) return true;
            else return false;
        }
        void NewGame()
        {
            iActualPlayer = 1;
            labelStatus.Text = "It's Player's 1 turn";
            labelStatus.BackColor = coPlayer1;
            // Alle PictureBoxen auf standart setzen!
            pictureBoxFeld1.BackColor = Color.White;
            pictureBoxFeld2.BackColor = Color.White;
            pictureBoxFeld3.BackColor = Color.White;
            pictureBoxFeld4.BackColor = Color.White;
            pictureBoxFeld5.BackColor = Color.White;
            pictureBoxFeld6.BackColor = Color.White;
            pictureBoxFeld7.BackColor = Color.White;
            pictureBoxFeld8.BackColor = Color.White;
            pictureBoxFeld9.BackColor = Color.White;
            // Alle PictureBoxen wieder enablen

            pictureBoxFeld1.Enabled = true;
            pictureBoxFeld2.Enabled = true;
            pictureBoxFeld3.Enabled = true;
            pictureBoxFeld4.Enabled = true;
            pictureBoxFeld5.Enabled = true;
            pictureBoxFeld6.Enabled = true;
            pictureBoxFeld7.Enabled = true;
            pictureBoxFeld8.Enabled = true;
            pictureBoxFeld9.Enabled = true;

            buttonNew.Enabled = false;
            buttonLoad.Enabled = false;
            buttonSave.Enabled = true;
            buttonStop.Enabled = true;

            ToolStripMenuItemNew.Enabled = false;
            ToolStripMenuItemLoad.Enabled = false;
            ToolStripMenuItemSave.Enabled = true;
            ToolStripMenuItemStop.Enabled = true;
        }
        void Stop()
        {
            pictureBoxFeld1.Enabled = false;
            pictureBoxFeld2.Enabled = false;
            pictureBoxFeld3.Enabled = false;
            pictureBoxFeld4.Enabled = false;
            pictureBoxFeld5.Enabled = false;
            pictureBoxFeld6.Enabled = false;
            pictureBoxFeld7.Enabled = false;
            pictureBoxFeld8.Enabled = false;
            pictureBoxFeld9.Enabled = false;

            buttonNew.Enabled = true;
            buttonLoad.Enabled = true;
            buttonSave.Enabled = false;
            buttonStop.Enabled = false;

            ToolStripMenuItemNew.Enabled = true;
            ToolStripMenuItemLoad.Enabled = true;
            ToolStripMenuItemSave.Enabled = false;
            ToolStripMenuItemStop.Enabled = false;
        }
        void GameSave()
        {
            SaveFileDialog GameSaveFileDialog = new SaveFileDialog();

            GameSaveFileDialog.Filter = "TicTacToeFiles (*.ttt)|*.ttt";
            GameSaveFileDialog.FilterIndex = 2;
            GameSaveFileDialog.RestoreDirectory = true;

            if (GameSaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                #region ifBefehleGameSaveFunction

                if (pictureBoxFeld1.BackColor == coPlayer1) { iZahl11 = 1; }
                if (pictureBoxFeld1.BackColor == coPlayer2) { iZahl11 = 2; }
                if (pictureBoxFeld1.BackColor == coComputer) { iZahl11 = 3; }

                if (pictureBoxFeld2.BackColor == coPlayer1) { iZahl12 = 1; }
                if (pictureBoxFeld2.BackColor == coPlayer2) { iZahl12 = 2; }
                if (pictureBoxFeld2.BackColor == coComputer) { iZahl12 = 3; }


                if (pictureBoxFeld3.BackColor == coPlayer1) { iZahl13 = 1; }
                if (pictureBoxFeld3.BackColor == coPlayer2) { iZahl13 = 2; }
                if (pictureBoxFeld3.BackColor == coComputer) { iZahl13 = 3; }

                if (pictureBoxFeld4.BackColor == coPlayer1) { iZahl21 = 1; }
                if (pictureBoxFeld4.BackColor == coPlayer2) { iZahl21 = 2; }
                if (pictureBoxFeld4.BackColor == coComputer) { iZahl21 = 3; }

                if (pictureBoxFeld5.BackColor == coPlayer1) { iZahl22 = 1; }
                if (pictureBoxFeld5.BackColor == coPlayer2) { iZahl22 = 2; }
                if (pictureBoxFeld5.BackColor == coComputer) { iZahl22 = 3; }

                if (pictureBoxFeld6.BackColor == coPlayer1) { iZahl23 = 1; }
                if (pictureBoxFeld6.BackColor == coPlayer2) { iZahl23 = 2; }
                if (pictureBoxFeld6.BackColor == coComputer) { iZahl23 = 3; }

                if (pictureBoxFeld7.BackColor == coPlayer1) { iZahl31 = 1; }
                if (pictureBoxFeld7.BackColor == coPlayer2) { iZahl31 = 2; }
                if (pictureBoxFeld7.BackColor == coComputer) { iZahl31 = 3; }

                if (pictureBoxFeld8.BackColor == coPlayer1) { iZahl32 = 1; }
                if (pictureBoxFeld8.BackColor == coPlayer2) { iZahl32 = 2; }
                if (pictureBoxFeld8.BackColor == coComputer) { iZahl32 = 3; }

                if (pictureBoxFeld9.BackColor == coPlayer1) { iZahl33 = 1; }
                if (pictureBoxFeld9.BackColor == coPlayer2) { iZahl33 = 2; }
                if (pictureBoxFeld9.BackColor == coComputer) { iZahl33 = 3; }

                #endregion
                StreamWriter myWriter = new StreamWriter(GameSaveFileDialog.FileName);
                myWriter.Write(iZahl11);
                myWriter.Write(iZahl12);
                myWriter.Write(iZahl13);
                myWriter.WriteLine("");
                myWriter.Write(iZahl21);
                myWriter.Write(iZahl22);
                myWriter.Write(iZahl23);
                myWriter.WriteLine("");
                myWriter.Write(iZahl31);
                myWriter.Write(iZahl32);
                myWriter.Write(iZahl33);
                myWriter.WriteLine("");
                myWriter.Write(iActualPlayer);
                if (iGameMode == 2)
                {
                    myWriter.WriteLine("");
                    myWriter.Write(iGameMode);
                }

                myWriter.Close();
            }
        }
        void GameLoad()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "TicTacToeFiles (*.ttt)|*.ttt";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;


            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamReader myReader = new StreamReader(openFileDialog.FileName);
                string strLine1, strLine2, strLine3, strLine4, strLine5;

                strLine1 = myReader.ReadLine();
                strLine2 = myReader.ReadLine();
                strLine3 = myReader.ReadLine();
                strLine4 = myReader.ReadLine();



                //  string.substring(int startIndex,int length)
                string strZahl11 = strLine1.Substring(0, 1);
                int iZahl11 = Convert.ToInt32(strZahl11);
                if (iZahl11 == 1) { pictureBoxFeld1.BackColor = coPlayer1; pictureBoxFeld1.Enabled = false; }
                if (iZahl11 == 2) { pictureBoxFeld1.BackColor = coPlayer2; pictureBoxFeld1.Enabled = false; }
                if (iZahl11 == 3) { pictureBoxFeld1.BackColor = coComputer; pictureBoxFeld1.Enabled = false; }
                if (iZahl11 == 0) { pictureBoxFeld1.BackColor = Color.White; pictureBoxFeld1.Enabled = true; }

                string strZahl12 = strLine1.Substring(1, 1);
                int iZahl12 = Convert.ToInt32(strZahl12);
                if (iZahl12 == 1) { pictureBoxFeld2.BackColor = coPlayer1; pictureBoxFeld2.Enabled = false; }
                if (iZahl12 == 2) { pictureBoxFeld2.BackColor = coPlayer2; pictureBoxFeld2.Enabled = false; }
                if (iZahl12 == 3) { pictureBoxFeld2.BackColor = coComputer; pictureBoxFeld2.Enabled = false; }
                if (iZahl12 == 0) { pictureBoxFeld2.BackColor = Color.White; pictureBoxFeld2.Enabled = true; }

                string strZahl13 = strLine1.Substring(2, 1);
                int iZahl13 = Convert.ToInt32(strZahl13);
                if (iZahl13 == 1) { pictureBoxFeld3.BackColor = coPlayer1; pictureBoxFeld3.Enabled = false; }
                if (iZahl13 == 2) { pictureBoxFeld3.BackColor = coPlayer2; pictureBoxFeld3.Enabled = false; }
                if (iZahl13 == 3) { pictureBoxFeld3.BackColor = coComputer; pictureBoxFeld3.Enabled = false; }
                if (iZahl13 == 0) { pictureBoxFeld3.BackColor = Color.White; pictureBoxFeld3.Enabled = true; }

                string strZahl21 = strLine2.Substring(0, 1);
                int iZahl21 = Convert.ToInt32(strZahl21);
                if (iZahl21 == 1) { pictureBoxFeld4.BackColor = coPlayer1; pictureBoxFeld4.Enabled = false; }
                if (iZahl21 == 2) { pictureBoxFeld4.BackColor = coPlayer2; pictureBoxFeld4.Enabled = false; }
                if (iZahl21 == 3) { pictureBoxFeld4.BackColor = coComputer; pictureBoxFeld4.Enabled = false; }
                if (iZahl21 == 0) { pictureBoxFeld4.BackColor = Color.White; pictureBoxFeld4.Enabled = true; }

                string strZahl22 = strLine2.Substring(1, 1);
                int iZahl22 = Convert.ToInt32(strZahl22);
                if (iZahl22 == 1) { pictureBoxFeld5.BackColor = coPlayer1; pictureBoxFeld5.Enabled = false; }
                if (iZahl22 == 2) { pictureBoxFeld5.BackColor = coPlayer2; pictureBoxFeld5.Enabled = false; }
                if (iZahl22 == 3) { pictureBoxFeld5.BackColor = coComputer; pictureBoxFeld5.Enabled = false; }
                if (iZahl22 == 0) { pictureBoxFeld5.BackColor = Color.White; pictureBoxFeld5.Enabled = true; }

                string strZahl23 = strLine2.Substring(2, 1);
                int iZahl23 = Convert.ToInt32(strZahl23);
                if (iZahl23 == 1) { pictureBoxFeld6.BackColor = coPlayer1; pictureBoxFeld6.Enabled = false; }
                if (iZahl23 == 2) { pictureBoxFeld6.BackColor = coPlayer2; pictureBoxFeld6.Enabled = false; }
                if (iZahl23 == 3) { pictureBoxFeld6.BackColor = coComputer; pictureBoxFeld6.Enabled = false; }
                if (iZahl23 == 0) { pictureBoxFeld6.BackColor = Color.White; pictureBoxFeld6.Enabled = true; }

                string strZahl31 = strLine3.Substring(0, 1);
                int iZahl31 = Convert.ToInt32(strZahl31);
                if (iZahl31 == 1) { pictureBoxFeld7.BackColor = coPlayer1; pictureBoxFeld7.Enabled = false; }
                if (iZahl31 == 2) { pictureBoxFeld7.BackColor = coPlayer2; pictureBoxFeld7.Enabled = false; }
                if (iZahl31 == 3) { pictureBoxFeld7.BackColor = coComputer; pictureBoxFeld7.Enabled = false; }
                if (iZahl31 == 0) { pictureBoxFeld7.BackColor = Color.White; pictureBoxFeld7.Enabled = true; }

                string strZahl32 = strLine3.Substring(1, 1);
                int iZahl32 = Convert.ToInt32(strZahl32);
                if (iZahl32 == 1) { pictureBoxFeld8.BackColor = coPlayer1; pictureBoxFeld8.Enabled = false; }
                if (iZahl32 == 2) { pictureBoxFeld8.BackColor = coPlayer2; pictureBoxFeld8.Enabled = false; }
                if (iZahl32 == 3) { pictureBoxFeld8.BackColor = coComputer; pictureBoxFeld8.Enabled = false; }
                if (iZahl32 == 0) { pictureBoxFeld8.BackColor = Color.White; pictureBoxFeld8.Enabled = true; }

                string strZahl33 = strLine3.Substring(2, 1);
                int iZahl33 = Convert.ToInt32(strZahl33);
                if (iZahl33 == 1) { pictureBoxFeld9.BackColor = coPlayer1; pictureBoxFeld9.Enabled = false; }
                if (iZahl33 == 2) { pictureBoxFeld9.BackColor = coPlayer2; pictureBoxFeld9.Enabled = false; }
                if (iZahl33 == 3) { pictureBoxFeld9.BackColor = coComputer; pictureBoxFeld9.Enabled = false; }
                if (iZahl33 == 0) { pictureBoxFeld9.BackColor = Color.White; pictureBoxFeld9.Enabled = true; ; }

                string strZahlStatus = strLine4.Substring(0, 1);
                int iZahlStatus = Convert.ToInt32(strZahlStatus);
                if (iZahlStatus == 1)
                {
                    iActualPlayer = 1;
                    coActualColor = coPlayer1;
                    labelStatus.BackColor = coPlayer1;
                    labelStatus.Text = "It's Player's 1 turn";
                }
                if (iZahlStatus == 2)
                {
                    iActualPlayer = 2;
                    coActualColor = coPlayer2;
                    labelStatus.BackColor = coPlayer2;
                    labelStatus.Text = "It's Player's 2 turn";
                }

                if (iGameMode == 2)
                {
                    strLine5 = myReader.ReadLine();
                    string strZahlGameMode = strLine5.Substring(0, 1);
                    int iZahlGameMode = Convert.ToInt32(strZahlGameMode);
                    if (iZahlGameMode == 2)
                    {
                        iGameMode = 2;
                        coActualColor = coPlayer1;
                        labelStatus.BackColor = coPlayer1;
                        labelStatus.Text = "It's Player's 1 turn";
                    }
                }
            }
            return;
        }
        void ComputerPlayer()
        {
            // Computer ist extrem dumm wurde aber nur schnell geschrieben ;)
            if (pictureBoxFeld5.BackColor == Color.White) { oActualComputerBox = pictureBoxFeld5; }
            else
            {
                if (pictureBoxFeld1.BackColor == Color.White) { oActualComputerBox = pictureBoxFeld1; }
                else
                {
                    if (pictureBoxFeld3.BackColor == Color.White) { oActualComputerBox = pictureBoxFeld3; }
                    else
                    {
                        if (pictureBoxFeld7.BackColor == Color.White) { oActualComputerBox = pictureBoxFeld7; }
                        else
                        {
                            if (pictureBoxFeld9.BackColor == Color.White) { oActualComputerBox = pictureBoxFeld9; }
                            else
                            {
                                if (pictureBoxFeld2.BackColor == Color.White) { oActualComputerBox = pictureBoxFeld2; }
                                else
                                {
                                    if (pictureBoxFeld6.BackColor == Color.White) { oActualComputerBox = pictureBoxFeld6; }
                                    else
                                    {
                                        if (pictureBoxFeld4.BackColor == Color.White) { oActualComputerBox = pictureBoxFeld4; }
                                        else
                                        {
                                            if (pictureBoxFeld8.BackColor == Color.White) { oActualComputerBox = pictureBoxFeld8; }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            oActualComputerBox.BackColor = coComputer;
            oActualComputerBox.Enabled = false;
        }


        #region PictureBoxClick
        private void pictureBoxFeld1_Click(object sender, EventArgs e)
        {
            ClickAction(pictureBoxFeld1);
        }

        private void pictureBoxFeld2_Click(object sender, EventArgs e)
        {
            ClickAction(pictureBoxFeld2);
        }

        private void pictureBoxFeld3_Click(object sender, EventArgs e)
        {
            ClickAction(pictureBoxFeld3);
        }

        private void pictureBoxFeld4_Click(object sender, EventArgs e)
        {
            ClickAction(pictureBoxFeld4);
        }

        private void pictureBoxFeld5_Click(object sender, EventArgs e)
        {
            ClickAction(pictureBoxFeld5);
        }

        private void pictureBoxFeld6_Click(object sender, EventArgs e)
        {
            ClickAction(pictureBoxFeld6);
        }

        private void pictureBoxFeld7_Click(object sender, EventArgs e)
        {
            ClickAction(pictureBoxFeld7);
        }

        private void pictureBoxFeld8_Click(object sender, EventArgs e)
        {
            ClickAction(pictureBoxFeld8);
        }

        private void pictureBoxFeld9_Click(object sender, EventArgs e)
        {
            ClickAction(pictureBoxFeld9);
        }

        #endregion
        #region ButtonsClicks

        private void buttonNew_Click(object sender, EventArgs e)
        {
            NewGame();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            Stop();
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            GameLoad();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            GameSave();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ToolStripMenuItemLoad_Click(object sender, EventArgs e)
        {
            GameLoad();
        }

        private void ToolStripMenuItemSave_Click(object sender, EventArgs e)
        {
            GameSave();
        }

        private void ToolStripMenuItemExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ToolStripMenuItemNew_Click(object sender, EventArgs e)
        {
            NewGame();
        }

        private void ToolStripMenuItemStop_Click(object sender, EventArgs e)
        {
            Stop();
        }

        private void ToolStripMenuItemSetup_Click(object sender, EventArgs e)
        {
            Stop();
            FormSetup = new FormSetup(this);
            FormSetup.ShowDialog();
            StatBoxes();
            NewGame();
        }

        private void ToolStripMenuItemAbout_Click(object sender, EventArgs e)
        {
            FormAbout.ShowDialog();
        }

        #endregion
    }
}