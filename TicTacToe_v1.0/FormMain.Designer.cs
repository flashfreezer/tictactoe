﻿namespace TicTacToe
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.pictureBoxFeld1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFeld4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFeld7 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFeld8 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFeld5 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFeld2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFeld9 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFeld6 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFeld3 = new System.Windows.Forms.PictureBox();
            this.groupBoxBoard = new System.Windows.Forms.GroupBox();
            this.groupBoxStatus = new System.Windows.Forms.GroupBox();
            this.labelStatus = new System.Windows.Forms.Label();
            this.buttonNew = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ToolStripMenuItemFile = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemSave = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemGame = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemNew = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemStop = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemSetup = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.labelPlayer1 = new System.Windows.Forms.Label();
            this.labelPlayer2 = new System.Windows.Forms.Label();
            this.pictureBoxPlayer1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxPlayer2 = new System.Windows.Forms.PictureBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld3)).BeginInit();
            this.groupBoxBoard.SuspendLayout();
            this.groupBoxStatus.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPlayer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPlayer2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxFeld1
            // 
            this.pictureBoxFeld1.BackColor = System.Drawing.Color.White;
            this.pictureBoxFeld1.Location = new System.Drawing.Point(6, 19);
            this.pictureBoxFeld1.Name = "pictureBoxFeld1";
            this.pictureBoxFeld1.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxFeld1.TabIndex = 0;
            this.pictureBoxFeld1.TabStop = false;
            this.pictureBoxFeld1.Tag = "[0,0]";
            this.pictureBoxFeld1.Click += new System.EventHandler(this.pictureBoxFeld1_Click);
            // 
            // pictureBoxFeld4
            // 
            this.pictureBoxFeld4.BackColor = System.Drawing.Color.White;
            this.pictureBoxFeld4.Location = new System.Drawing.Point(6, 89);
            this.pictureBoxFeld4.Name = "pictureBoxFeld4";
            this.pictureBoxFeld4.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxFeld4.TabIndex = 1;
            this.pictureBoxFeld4.TabStop = false;
            this.pictureBoxFeld4.Tag = "[1,0]";
            this.pictureBoxFeld4.Click += new System.EventHandler(this.pictureBoxFeld4_Click);
            // 
            // pictureBoxFeld7
            // 
            this.pictureBoxFeld7.BackColor = System.Drawing.Color.White;
            this.pictureBoxFeld7.Location = new System.Drawing.Point(6, 159);
            this.pictureBoxFeld7.Name = "pictureBoxFeld7";
            this.pictureBoxFeld7.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxFeld7.TabIndex = 2;
            this.pictureBoxFeld7.TabStop = false;
            this.pictureBoxFeld7.Tag = "[2,0]";
            this.pictureBoxFeld7.Click += new System.EventHandler(this.pictureBoxFeld7_Click);
            // 
            // pictureBoxFeld8
            // 
            this.pictureBoxFeld8.BackColor = System.Drawing.Color.White;
            this.pictureBoxFeld8.Location = new System.Drawing.Point(76, 159);
            this.pictureBoxFeld8.Name = "pictureBoxFeld8";
            this.pictureBoxFeld8.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxFeld8.TabIndex = 5;
            this.pictureBoxFeld8.TabStop = false;
            this.pictureBoxFeld8.Tag = "[2,1]";
            this.pictureBoxFeld8.Click += new System.EventHandler(this.pictureBoxFeld8_Click);
            // 
            // pictureBoxFeld5
            // 
            this.pictureBoxFeld5.BackColor = System.Drawing.Color.White;
            this.pictureBoxFeld5.Location = new System.Drawing.Point(76, 89);
            this.pictureBoxFeld5.Name = "pictureBoxFeld5";
            this.pictureBoxFeld5.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxFeld5.TabIndex = 4;
            this.pictureBoxFeld5.TabStop = false;
            this.pictureBoxFeld5.Tag = "[1,1]";
            this.pictureBoxFeld5.Click += new System.EventHandler(this.pictureBoxFeld5_Click);
            // 
            // pictureBoxFeld2
            // 
            this.pictureBoxFeld2.BackColor = System.Drawing.Color.White;
            this.pictureBoxFeld2.Location = new System.Drawing.Point(76, 19);
            this.pictureBoxFeld2.Name = "pictureBoxFeld2";
            this.pictureBoxFeld2.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxFeld2.TabIndex = 3;
            this.pictureBoxFeld2.TabStop = false;
            this.pictureBoxFeld2.Tag = "[0;1]";
            this.pictureBoxFeld2.Click += new System.EventHandler(this.pictureBoxFeld2_Click);
            // 
            // pictureBoxFeld9
            // 
            this.pictureBoxFeld9.BackColor = System.Drawing.Color.White;
            this.pictureBoxFeld9.Location = new System.Drawing.Point(146, 159);
            this.pictureBoxFeld9.Name = "pictureBoxFeld9";
            this.pictureBoxFeld9.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxFeld9.TabIndex = 8;
            this.pictureBoxFeld9.TabStop = false;
            this.pictureBoxFeld9.Tag = "[2,2]";
            this.pictureBoxFeld9.Click += new System.EventHandler(this.pictureBoxFeld9_Click);
            // 
            // pictureBoxFeld6
            // 
            this.pictureBoxFeld6.BackColor = System.Drawing.Color.White;
            this.pictureBoxFeld6.Location = new System.Drawing.Point(146, 89);
            this.pictureBoxFeld6.Name = "pictureBoxFeld6";
            this.pictureBoxFeld6.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxFeld6.TabIndex = 7;
            this.pictureBoxFeld6.TabStop = false;
            this.pictureBoxFeld6.Tag = "[1,2]";
            this.pictureBoxFeld6.Click += new System.EventHandler(this.pictureBoxFeld6_Click);
            // 
            // pictureBoxFeld3
            // 
            this.pictureBoxFeld3.BackColor = System.Drawing.Color.White;
            this.pictureBoxFeld3.Location = new System.Drawing.Point(146, 19);
            this.pictureBoxFeld3.Name = "pictureBoxFeld3";
            this.pictureBoxFeld3.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxFeld3.TabIndex = 6;
            this.pictureBoxFeld3.TabStop = false;
            this.pictureBoxFeld3.Tag = "[0;2]";
            this.pictureBoxFeld3.Click += new System.EventHandler(this.pictureBoxFeld3_Click);
            // 
            // groupBoxBoard
            // 
            this.groupBoxBoard.Controls.Add(this.pictureBoxFeld1);
            this.groupBoxBoard.Controls.Add(this.pictureBoxFeld9);
            this.groupBoxBoard.Controls.Add(this.pictureBoxFeld4);
            this.groupBoxBoard.Controls.Add(this.pictureBoxFeld6);
            this.groupBoxBoard.Controls.Add(this.pictureBoxFeld7);
            this.groupBoxBoard.Controls.Add(this.pictureBoxFeld3);
            this.groupBoxBoard.Controls.Add(this.pictureBoxFeld2);
            this.groupBoxBoard.Controls.Add(this.pictureBoxFeld8);
            this.groupBoxBoard.Controls.Add(this.pictureBoxFeld5);
            this.groupBoxBoard.Location = new System.Drawing.Point(12, 80);
            this.groupBoxBoard.Name = "groupBoxBoard";
            this.groupBoxBoard.Size = new System.Drawing.Size(223, 235);
            this.groupBoxBoard.TabIndex = 9;
            this.groupBoxBoard.TabStop = false;
            this.groupBoxBoard.Text = "Board";
            // 
            // groupBoxStatus
            // 
            this.groupBoxStatus.Controls.Add(this.labelStatus);
            this.groupBoxStatus.Location = new System.Drawing.Point(12, 27);
            this.groupBoxStatus.Name = "groupBoxStatus";
            this.groupBoxStatus.Size = new System.Drawing.Size(584, 47);
            this.groupBoxStatus.TabIndex = 10;
            this.groupBoxStatus.TabStop = false;
            this.groupBoxStatus.Text = "Status";
            // 
            // labelStatus
            // 
            this.labelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus.Location = new System.Drawing.Point(7, 16);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(561, 28);
            this.labelStatus.TabIndex = 0;
            this.labelStatus.Text = "Press New Game";
            this.labelStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonNew
            // 
            this.buttonNew.Location = new System.Drawing.Point(258, 102);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(322, 35);
            this.buttonNew.TabIndex = 11;
            this.buttonNew.Text = "New Game";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(258, 143);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(322, 35);
            this.buttonStop.TabIndex = 12;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Location = new System.Drawing.Point(258, 184);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(322, 35);
            this.buttonLoad.TabIndex = 13;
            this.buttonLoad.Text = "Load";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(258, 225);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(322, 35);
            this.buttonSave.TabIndex = 14;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(258, 268);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(322, 35);
            this.buttonExit.TabIndex = 15;
            this.buttonExit.Text = "Quit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemFile,
            this.ToolStripMenuItemGame,
            this.ToolStripMenuItemSetup,
            this.ToolStripMenuItemAbout});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(602, 24);
            this.menuStrip1.TabIndex = 16;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ToolStripMenuItemFile
            // 
            this.ToolStripMenuItemFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemLoad,
            this.ToolStripMenuItemSave,
            this.ToolStripMenuItemExit});
            this.ToolStripMenuItemFile.Name = "ToolStripMenuItemFile";
            this.ToolStripMenuItemFile.Size = new System.Drawing.Size(37, 20);
            this.ToolStripMenuItemFile.Text = "File";
            // 
            // ToolStripMenuItemLoad
            // 
            this.ToolStripMenuItemLoad.Name = "ToolStripMenuItemLoad";
            this.ToolStripMenuItemLoad.Size = new System.Drawing.Size(149, 22);
            this.ToolStripMenuItemLoad.Text = "Load";
            this.ToolStripMenuItemLoad.Click += new System.EventHandler(this.ToolStripMenuItemLoad_Click);
            // 
            // ToolStripMenuItemSave
            // 
            this.ToolStripMenuItemSave.Name = "ToolStripMenuItemSave";
            this.ToolStripMenuItemSave.Size = new System.Drawing.Size(149, 22);
            this.ToolStripMenuItemSave.Text = "Save";
            this.ToolStripMenuItemSave.Click += new System.EventHandler(this.ToolStripMenuItemSave_Click);
            // 
            // ToolStripMenuItemExit
            // 
            this.ToolStripMenuItemExit.Name = "ToolStripMenuItemExit";
            this.ToolStripMenuItemExit.Size = new System.Drawing.Size(149, 22);
            this.ToolStripMenuItemExit.Text = "Quit (Alt + F4)";
            this.ToolStripMenuItemExit.Click += new System.EventHandler(this.ToolStripMenuItemExit_Click);
            // 
            // ToolStripMenuItemGame
            // 
            this.ToolStripMenuItemGame.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemNew,
            this.ToolStripMenuItemStop});
            this.ToolStripMenuItemGame.Name = "ToolStripMenuItemGame";
            this.ToolStripMenuItemGame.Size = new System.Drawing.Size(50, 20);
            this.ToolStripMenuItemGame.Text = "Game";
            // 
            // ToolStripMenuItemNew
            // 
            this.ToolStripMenuItemNew.Name = "ToolStripMenuItemNew";
            this.ToolStripMenuItemNew.Size = new System.Drawing.Size(98, 22);
            this.ToolStripMenuItemNew.Text = "New";
            this.ToolStripMenuItemNew.Click += new System.EventHandler(this.ToolStripMenuItemNew_Click);
            // 
            // ToolStripMenuItemStop
            // 
            this.ToolStripMenuItemStop.Name = "ToolStripMenuItemStop";
            this.ToolStripMenuItemStop.Size = new System.Drawing.Size(98, 22);
            this.ToolStripMenuItemStop.Text = "Stop";
            this.ToolStripMenuItemStop.Click += new System.EventHandler(this.ToolStripMenuItemStop_Click);
            // 
            // ToolStripMenuItemSetup
            // 
            this.ToolStripMenuItemSetup.Name = "ToolStripMenuItemSetup";
            this.ToolStripMenuItemSetup.Size = new System.Drawing.Size(49, 20);
            this.ToolStripMenuItemSetup.Text = "Setup";
            this.ToolStripMenuItemSetup.Click += new System.EventHandler(this.ToolStripMenuItemSetup_Click);
            // 
            // ToolStripMenuItemAbout
            // 
            this.ToolStripMenuItemAbout.Name = "ToolStripMenuItemAbout";
            this.ToolStripMenuItemAbout.Size = new System.Drawing.Size(52, 20);
            this.ToolStripMenuItemAbout.Text = "About";
            this.ToolStripMenuItemAbout.Click += new System.EventHandler(this.ToolStripMenuItemAbout_Click);
            // 
            // labelPlayer1
            // 
            this.labelPlayer1.Location = new System.Drawing.Point(286, 81);
            this.labelPlayer1.Name = "labelPlayer1";
            this.labelPlayer1.Size = new System.Drawing.Size(88, 15);
            this.labelPlayer1.TabIndex = 17;
            this.labelPlayer1.Text = "Player 1";
            // 
            // labelPlayer2
            // 
            this.labelPlayer2.Location = new System.Drawing.Point(492, 80);
            this.labelPlayer2.Name = "labelPlayer2";
            this.labelPlayer2.Size = new System.Drawing.Size(88, 15);
            this.labelPlayer2.TabIndex = 18;
            this.labelPlayer2.Text = "Player 2";
            // 
            // pictureBoxPlayer1
            // 
            this.pictureBoxPlayer1.Location = new System.Drawing.Point(267, 80);
            this.pictureBoxPlayer1.Name = "pictureBoxPlayer1";
            this.pictureBoxPlayer1.Size = new System.Drawing.Size(15, 15);
            this.pictureBoxPlayer1.TabIndex = 19;
            this.pictureBoxPlayer1.TabStop = false;
            // 
            // pictureBoxPlayer2
            // 
            this.pictureBoxPlayer2.Location = new System.Drawing.Point(471, 80);
            this.pictureBoxPlayer2.Name = "pictureBoxPlayer2";
            this.pictureBoxPlayer2.Size = new System.Drawing.Size(15, 15);
            this.pictureBoxPlayer2.TabIndex = 20;
            this.pictureBoxPlayer2.TabStop = false;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 318);
            this.Controls.Add(this.pictureBoxPlayer2);
            this.Controls.Add(this.pictureBoxPlayer1);
            this.Controls.Add(this.labelPlayer2);
            this.Controls.Add(this.labelPlayer1);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonLoad);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonNew);
            this.Controls.Add(this.groupBoxStatus);
            this.Controls.Add(this.groupBoxBoard);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "Tic Tac Toe";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFeld3)).EndInit();
            this.groupBoxBoard.ResumeLayout(false);
            this.groupBoxStatus.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPlayer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPlayer2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxFeld1;
        private System.Windows.Forms.PictureBox pictureBoxFeld4;
        private System.Windows.Forms.PictureBox pictureBoxFeld7;
        private System.Windows.Forms.PictureBox pictureBoxFeld8;
        private System.Windows.Forms.PictureBox pictureBoxFeld5;
        private System.Windows.Forms.PictureBox pictureBoxFeld2;
        private System.Windows.Forms.PictureBox pictureBoxFeld9;
        private System.Windows.Forms.PictureBox pictureBoxFeld6;
        private System.Windows.Forms.PictureBox pictureBoxFeld3;
        private System.Windows.Forms.GroupBox groupBoxBoard;
        private System.Windows.Forms.GroupBox groupBoxStatus;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemFile;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemLoad;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemSave;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemExit;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemGame;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemNew;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemStop;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemSetup;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemAbout;
        private System.Windows.Forms.Label labelPlayer1;
        private System.Windows.Forms.Label labelPlayer2;
        private System.Windows.Forms.PictureBox pictureBoxPlayer1;
        private System.Windows.Forms.PictureBox pictureBoxPlayer2;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}

